/******************************************************************************
 *
 *  FILE
 *      bma250_accelerometer_sensor.h
 *
 *  DESCRIPTION
 *      Header file for BMA250e Accelerometer Sensor
 *
 *****************************************************************************/

#ifndef __BMA250_ACCELEROMETER_SENSOR_H__
#define __BMA250_ACCELEROMETER_SENSOR_H__

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <types.h>

/*============================================================================*
 *  local Header Files
 *============================================================================*/
#include "user_config.h"
#include "i2c_comms.h"

#ifdef ACCELEROMETER_SENSOR_BMA250
/*============================================================================*
 *  Public Definitions
 *============================================================================*/
/* Accelerometer sensor BMA250 specific definitions */
#define BMA250_0_I2C_ADDRESS           (0x30)
#define BMA250_1_I2C_ADDRESS           (0x31)

#define BMA250_I2C_ADDRESS             (BMA250_0_I2C_ADDRESS)

/* Manufacturer ID for BMA250. */
#define BMA250_MANU_ID                 (0x03)   // 0x03 (BMA250)  0xF9 (BMA250E)

/* BMA250 I2C registers */
#define REG_MANU_ID                     (0x00)
#define REG_RANGE                       (0x0F)
#define REG_BANDWIDTH                   (0x10)
#define REG_LOWPOWERMODE                (0x11)
#define REG_SOFTRESET                   (0x14)
#define REG_INT_EN_0                    (0x16)
#define REG_INT_MAP_0                   (0x19)
#define REG_INT_MAP_2                   (0x1B)
#define REG_INT_SRC                     (0x1E)
#define REG_INT_OUT_CTR                 (0x20)
#define REG_INT_RST_LATCH               (0x21)
#define REG_DTAP_TIME                   (0x2A)
#define REG_DTAP_SAMPLES                (0x2B)


/*============================================================================*
 *  Public Function Prototypes
 *============================================================================*/
/* This function initialises the accelerometer sensor BMA250 */
extern bool BMA250_Init(void);

/* This function handles the interrupt from accelerometer sensor BMA250 */
extern void BMA250_InterruptHandler(void);

#endif /* ACCELEROMETER_SENSOR_BMA250 */
#endif /* __BMA250_ACCELEROMETER_SENSOR_H__ */

