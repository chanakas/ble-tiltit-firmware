/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2013-2014
 *  Part of CSR uEnergy SDK 2.4.3
 *  Application version 2.4.3.0
 *
 *  FILE
 *      hw_access.h
 *
 *  DESCRIPTION
 *      Header definitions for HW setup.
 *
 *****************************************************************************/

#ifndef __HW_ACCESS_H__
#define __HW_ACCESS_H__

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <sys_events.h>     /* System Event definitions and declarations */

/*============================================================================*
 *  Public Definitions
 *============================================================================*/

/* Convert a PIO number into a bit mask */
#define PIO_BIT_MASK(pio)       (0x01UL << (pio))

/* PIO direction */
#define PIO_DIRECTION_INPUT     (FALSE)
#define PIO_DIRECTION_OUTPUT    (TRUE)

/* PIO state */
#define PIO_STATE_HIGH          (TRUE)
#define PIO_STATE_LOW           (FALSE)

#define LED_PWM         (0)
#define LED_PIO         (9)
#define PUSH_SW_PIO      (0)
#define INT2_PIO        (1)
#define INT1_PIO        (4)

#define PUSH_SW_MASK  PIO_BIT_MASK(PUSH_SW_PIO)
#define INT2_MASK     PIO_BIT_MASK(INT2_PIO)
#define INT1_MASK     PIO_BIT_MASK(INT1_PIO)

/* Bit-mask of all the Switch PIOs used by the board. */
#define ALL_BIT_MASK    (PUSH_SW_MASK | INT2_MASK | INT1_MASK )

#define BUTTON_BIT_MASK    (PUSH_SW_MASK)
#define ACCEL_BIT_MASK    (INT2_MASK)

/*============================================================================*
 *  Public Function Prototypes
 *============================================================================*/

/* This function initialises the LED line. */
extern void IOTLightControlDeviceInit(void);

/* This function sets the Power State of Light. */
extern void IOTLightControlDevicePower(bool power_on);

/* This function Blinks LED */
extern void IOTLightBlink(void);

/* Initialise the application hardware */
extern void InitHardware(void);

/* Initialise the application hardware data structure */
extern void HwDataInit(void);

/* Reset the application hardware data structure */
extern void HwDataReset(void);

/* Handle the PIO changed event */
extern void HandlePIOChangedEvent(uint32 pio_changed);

#endif /* __HW_ACCESS_H__ */
